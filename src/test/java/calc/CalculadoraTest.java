package calc;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculadoraTest {
    private Calculadora calc;
    @BeforeEach
    void setUp() {
        calc = new Calculadora();
    }

    @AfterEach
    void tearDown() {
    }

    /**
     * Verifica se o registrador o get e set do registrador funciona.
     */
    @Test
    void testRegistrador() {
        calc.setRegistrador(10);
        Assertions.assertEquals(10, calc.getRegistrador());
        calc.setRegistrador(14);
        Assertions.assertEquals(14, calc.getRegistrador());
    }

    /**
     * Verifica se o registrador é 0 após a execução do clear.
     */
    @Test
    void clear() {
        calc.setRegistrador(10);
        Assertions.assertNotEquals(0, calc.getRegistrador());
        calc.clear();
        // o teste abaixo falha, precisamos implementar clear() para que o teste de certo.
        Assertions.assertEquals(0, calc.getRegistrador());
    }

    /**
     * Verifica soma de dois valores positivos
     * Soma deve devolver o resultado da operação e o registrador deve conter o valor do resultado
     */
    @Test
    void somaSimples() {
        calc.setRegistrador(111);
        // primeiro vamos ter certeza que o valor no registrador está como queremos
        Assertions.assertEquals(111, calc.getRegistrador());
        // os testes abaixo falham, precisamos implementar soma() para que o teste de certo.
        // primeira parte do teste, o retorno deve ser o valor da soma
        Assertions.assertEquals(333, calc.soma(222));
        // agora verificamos que o resultado foi armazenado no registrador.
        Assertions.assertEquals(333, calc.getRegistrador());
    }

    @Test
    void subtrai() {
    }

    @Test
    void multiplica() {
    }

    @Test
    void divide() {
    }

    @Test
    void raizQuadrada() {
    }
}