package calc;

public class Calculadora {
    private double registrador = 0;
    private double memoria = 0;

    /**
     * Obtém o valor do registrador.
     */
    public double getRegistrador() {
        return registrador;
    }

    /**
     * Armazena um valor no registrador.
     * @param valor valor a ser atribuido.
     */
    public void setRegistrador(double valor) {
        registrador = valor;
    }
    /**
     * Atribui 0 ao registrador.
     */
    public void clear() {
    }

    /**
     * Soma um valor ao registrador.
     * @param valor valor a ser somado.
     * @return o resultado da soma.
     */
    public double soma(double valor) {
        return 0;
    }

    /**
     * Subtrai um valor do registrador.
     * @param valor valor a ser subtraido.
     * @return o resultado da subtração.
     */
    public double subtrai(double valor) {
        return 0;
    }

    /**
     * Subtrai um valor do registrador.
     * @param valor valor a ser subtraido.
     * @return o resultado da subtração.
     */
    public double multiplica(double valor) {
        return 0;
    }

    /**
     * Divide o registrador pelo valor.
     * @param valor dividendo.
     * @return o resultado da divisão.
     */
    public double divide(double valor) {
        return 0;
    }

    /**
     * Calcula a raiz quadrada do valor e armazena no registrador.
     * @param valor operando.
     * @return resultado da raiz quadrada do valor fornecido.
     */
    public double raizQuadrada(double valor) {
        return 0;
    }
}
